import com.vdurmont.emoji._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.countDistinct
import org.apache.spark.sql.functions._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import scala.collection.JavaConverters._

object App {
  val dir = "C:\\bg\\bgdata_small"
  val spark = SparkSession.builder()
    .appName("SimpleStatistic")
    .master("local[*]")
    .getOrCreate()
  val showLimit = 8;

  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "C:\\hadoop")
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
    saveTcv()
  }
  def saveDatasets(): Unit =
  {

    //    Logger.getLogger("org").setLevel(Level.ERROR)
    //    Logger.getLogger("akka").setLevel(Level.ERROR)

    System.setProperty("hadoop.home.dir", "C:\\hadoop")
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    println("Hello scala")
    val dir = "C:\\bg\\bgdata_small"

    val spark = SparkSession.builder()
    .appName("SimpleStatistic")
    .master("local[*]")
    .getOrCreate()


    val posts = spark.read.parquet(s"$dir\\userWallPosts.parquet")

    posts.show(8)

    val all_posts = posts.groupBy("from_id").count()
    val all_posts_names = Seq("from_id", "all_posts_count")

    val original_posts = posts.filter(posts("from_id") === posts("owner_id")).groupBy("from_id").count()
    val original_posts_names = Seq("from_id", "original_posts_count")

    val all_posts_renamed = all_posts.toDF(all_posts_names: _*)
    val original_posts_renamed = original_posts.toDF(original_posts_names: _*)

    original_posts_renamed.show(8)

    //comments

    val merged = all_posts_renamed.join(original_posts_renamed, Seq("from_id"))

    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")
    val all_comments = comments.groupBy("post_owner_id").count()
    val all_comments_names = Seq("from_id", "all_comments_count")
    val all_comments_renamed = all_comments.toDF(all_comments_names: _*)

    val merged2 = merged.join(all_comments_renamed, Seq("from_id"))

    //likes

    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val all_likes = likes.groupBy("likerId").count()
    val all_likes_names = Seq("from_id", "all_likes_count")
    val all_likes_renamed = all_likes.toDF(all_likes_names: _*)

    val merged3 = merged2.join(all_likes_renamed, Seq("from_id"))

    merged3.show(50)

    merged3.write.parquet("comments_likes.parquet")
    val friends = spark.read.parquet(s"$dir\\friends.parquet")
    //val followers = spark.read.parquet(s"$dir\\followers.parquet")
    val groups = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")


    val friends_num = friends.groupBy("profile").count()
    //val followers_num = followers.groupBy("profile").count()
    val groups_num = groups.groupBy("user").count()

    val all_friends_names = Seq("from_id", "all_friends_count")
    //val all_followers_names = Seq("from_id", "all_followers_count")
    val all_groups_names = Seq("from_id", "all_groups_count")

    val all_friends_renamed = friends_num.toDF(all_friends_names: _*)
    //val all_followers_renamed = followers_num.toDF(all_followers_names: _*)
    val all_groups_renamed = groups_num.toDF(all_groups_names: _*)

    val merged = all_groups_renamed.join(all_friends_renamed, Seq("from_id"))
    //val merged2 = merged.join(all_groups_renamed, Seq("from_id"))

    merged.show(50)

    merged.write.parquet("friends_groups.parquet")




  }

  def saveTcv(): Unit ={
    import org.apache.spark.mllib.feature.PCA
    import org.apache.spark.mllib.linalg.Vectors
    import org.apache.spark.mllib.regression.LabeledPoint
    import org.apache.spark.rdd.RDD

    val commentsLikes = spark.read.parquet("comments_likes.parquet")
      .filter(col("all_comments_count") > 0 &&
        col("all_likes_count") > 0 &&
        col("original_posts_count") > 0)
      .sort(col("from_id"))
    val friendsGroups = spark.read.parquet("friends_groups.parquet")
      .filter(col("all_groups_count") > 0 &&
        col("all_friends_count") > 0)

    val dataset = friendsGroups.join(commentsLikes, Seq("from_id"))
      .na.fill(0).toDF()
    val features = dataset.drop("from_id").toDF()
    features.show(showLimit)
    //features.write.csv("dataForPCA", sep='\t')
    features
      .repartition(1)
      .write.format("com.databricks.spark.csv")
      .option("header", "true")
      .save("mydata.csv")



    //features.write.format("com.databricks.spark.csv").save("features.csv")
    //features.write.csv("features.csv")
  }

  def viewMLKMeans(): Unit = {
    import spark.implicits._
    import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
    import org.apache.spark.mllib.linalg.Vectors
    org.apache.spark.sql.SQLContext
    import org.apache.spark.mllib.feature.Normalizer

    val commentsLikes = spark.read.parquet("comments_likes.parquet")
      .filter(col("all_comments_count") > 0 &&
        col("all_likes_count") > 0 &&
        col("original_posts_count") > 0)
      .sort(col("from_id"))
    val friendsGroups = spark.read.parquet("friends_groups.parquet")
      .filter(col("all_groups_count") > 0 &&
        col("all_friends_count") > 0)

    val dataset = friendsGroups.join(commentsLikes, Seq("from_id"))
      .na.fill(0).toDF()
    dataset.show(showLimit)


    val features = dataset.drop("from_id")
    val vectorFeatures = features.rdd
      .map(r => {
        Vectors.parse(r.toString())
      })
      .cache()

    //val normalizer = new Normalizer()
    //val transformed = normalizer.transform(vectorFeatures).cache()

    val numClusters = 3
    val numIterations = 20
    val clusters = KMeans.train(vectorFeatures, numClusters, numIterations)
    val WSSSE = clusters.computeCost(vectorFeatures)
    println(s"Within Set Sum of Squared Errors = $WSSSE")
    val userCluster = clusters.predict(vectorFeatures)
      .toDF()

    println(userCluster.count())
    println(dataset.count())


    val userClusterForJoin = userCluster.withColumn("incremental_id", monotonically_increasing_id())
    val datasetForJoin = dataset.withColumn("incremental_id1", monotonically_increasing_id())
    val result = userClusterForJoin.join(datasetForJoin, col("incremental_id") === col("incremental_id1"), "left")
      .drop("incremental_id")
      .drop("incremental_id1")
    result.show(showLimit)

    clusters.clusterCenters.zipWithIndex.foreach { r =>
      println("Center cluster " + r._2 + " " + r._1)
      result.filter("value=" + r._2.toString).describe().show()
    }
  }

  def viewMLBisectingKMeans(): Unit = {
    import spark.implicits._
    import org.apache.spark.mllib.clustering.BisectingKMeans
    import org.apache.spark.mllib.linalg.Vectors
    org.apache.spark.sql.SQLContext
    import org.apache.spark.mllib.feature.Normalizer

    val commentsLikes = spark.read.parquet("comments_likes.parquet")
      .filter(col("all_comments_count") > 0 &&
        col("all_likes_count") > 0 &&
        col("original_posts_count") > 0)
      .sort(col("from_id"))
    val friendsGroups = spark.read.parquet("friends_groups.parquet")
      .filter(col("all_groups_count") > 0 &&
        col("all_friends_count") > 0)

    val dataset = friendsGroups.join(commentsLikes, Seq("from_id"))
      .na.fill(0).toDF()
    dataset.show(showLimit)


    val features = dataset.drop("from_id")
    val vectorFeatures = features.rdd
      .map(r => {
        Vectors.parse(r.toString())
      })
      .cache()

    //val normalizer = new Normalizer()
    //val transformed = normalizer.transform(vectorFeatures).cache()

    val bkm = new BisectingKMeans().setK(6)
    val clusters = bkm.run(vectorFeatures)

    val WSSSE = clusters.computeCost(vectorFeatures)
    println(s"Within Set Sum of Squared Errors = $WSSSE")
    val userCluster = clusters.predict(vectorFeatures)
      .toDF()

    println(userCluster.count())
    println(dataset.count())


    val userClusterForJoin = userCluster.withColumn("incremental_id", monotonically_increasing_id())
    val datasetForJoin = dataset.withColumn("incremental_id1", monotonically_increasing_id())
    val result = userClusterForJoin.join(datasetForJoin, col("incremental_id") === col("incremental_id1"), "left")
      .drop("incremental_id")
      .drop("incremental_id1")
    result.show(showLimit)

    clusters.clusterCenters.zipWithIndex.foreach { r =>
      println("Center cluster " + r._2 + " " + r._1)
      result.filter("value=" + r._2.toString).describe().show()
    }
  }

  def viewAboutUser(userId: Int): Unit = {
    //1) count of comments, posts (all), original posts, reposts and likes made by user
    /*viewCountComments(userId)
    viewCountAllPosts(userId)
    viewCountOriginalPosts(userId)
    viewCountReposts(userId)
    viewCountLikes(userId)
    //2) count of friends, groups, followers
    viewCountFriends(userId)
    viewCountFriends(userId)
    viewCountGroups(userId)
    //viewCountFollowers(userId)

    //3) count of videos, audios, photos, gifts
    viewCountVideos(userId)
    viewCountAudios(userId)
    viewCountPhotos(userId)
    viewCountGifts(userId)

    //4) count of "incoming" (made by other users) comments, max and mean "incoming" comments per post
    viewCountIncomingCommentsOtherUsers(userId)
    viewMaxIncomingCommentsPerPost(userId)
    viewMeanIncomingCommentsPerPost(userId)

    //5) count of "incoming" likes, max and mean "incoming" likes per post
    viewCountIncomingLikesPerPost(userId)
    viewMaxIncomingLikesPerPost(userId)
    viewMeanIncomingLikesPerPost(userId)

    //6) count of geo tagged posts
    viewCountGeoTaggedPosts(userId)
    viewCountPublicGroupsForUser(userId)
    viewCountPrivateGroupsForUser(userId)

    Medium level:

4) aggregate (e.g. count, max, mean) characteristics for comments and likes (separtely) made by (a) friends and (b) followers per user
5) find emoji (separately, count of: all, negative, positive, others) in (a) user's posts (b) user's comments
*/
    //1) count of reposts from subscribed and not-subscribed groups
    //viewCountRepostsFromSubscribedGroups(userId)
    //viewCountRepostsFromNotSubscribedGroups(userId)

    //2) count of deleted users in friends and followers
    //viewCountDeletedUsersInFriendsAndFollowers(2176592)

    //3) aggregate (e.g. count, max, mean) characteristics for comments and likes (separtely) made by (a) friends and (b) followers per post
    //viewAggregateCommentsPerPost(282061277)
    //viewAggregateLikesPerPost(282061277)

    //4) aggregate (e.g. count, max, mean) characteristics for comments and likes (separtely) made by (a) friends and (b) followers per user
    //viewAggregateCommentsPerUser(282061277)
    //viewAggregateLikesPerUser(282061277)

    //5) find emoji (separately, count of: all, negative, positive, others) in (a) user's posts (b) user's comments
    //viewEmoji(282061277)
    //viewML()
  }

  def viewCountRepostsFromSubscribedGroups(userId: Int) {
    println("viewCountRepostsFromSubscribedGroups")

    val posts = spark.read.parquet(s"$dir\\userWallPosts.parquet")
    val posts_filt = posts
      .filter(posts("is_reposted"))
      .filter(posts("repost_info.orig_owner_id") < 0)
      .withColumn("tmp", concat(col("from_id"), lit("_"), col("repost_info.orig_owner_id")))
    posts_filt.show(showLimit)

    val user_groups = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")
      .withColumnRenamed("key", "tmp")

    val with_sub_reposts = posts_filt.join(user_groups, Seq("tmp"))
      .filter(col("from_id") === userId)
      .groupBy("from_id")
      .agg(count("from_id").alias("reposts_sub_num"))

    with_sub_reposts.show(showLimit)

  }

  def viewCountRepostsFromNotSubscribedGroups(userId: Int) {
    println("viewCountRepostsFromNotSubscribedGroups")

    val posts = spark.read.parquet(s"$dir\\userWallPosts.parquet")

    val posts_filt = posts
      .filter(posts("is_reposted"))
      .filter(posts("repost_info.orig_owner_id") < 0)
      .withColumn("tmp", concat(col("from_id"), lit("_"), col("repost_info.orig_owner_id")))
    posts_filt.show(showLimit) // Can not read value at 0 in block -1 in file file:/C:/bg/bgdata_small/userWallPosts.parquet/part-00149-9ac34d75-dbe3-47ca-84a7-da6c419ed515-c000.snappy.parquet


    val user_groups = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")
      .withColumnRenamed("key", "tmp")

    val without_sub_reposts = posts_filt.join(user_groups, Seq("tmp"), "leftanti")
      .filter(col("from_id") === userId)
      .groupBy("from_id")
      .agg(count("from_id").alias("reposts_sub_num"))
    without_sub_reposts.show(showLimit)
  }

  def viewCountDeletedUsersInFriendsAndFollowers(userId: Int) {
    println("viewCountDeletedUsersInFriendsAndFollowers")
    //could not read page Page [bytes.size=1048593, valueCount=51796, uncompressedSize=1048593] in col [key] BINARY
    val friendsProfiles = spark.read.parquet(s"$dir\\friendsProfiles.parquet")
    val friends = spark.read.parquet(s"$dir\\friends.parquet")
      .filter(col("profile") === userId)

    var deactiveProfiles = friendsProfiles.filter(col("deactivated").isNull)
    friends.join(deactiveProfiles, deactiveProfiles("id") === friends("profile"))
      .groupBy("profile")
      .count()
      .show(showLimit)

    var activeProfiles = friendsProfiles.filter(friendsProfiles("deactivated").isNotNull)
    friends.join(activeProfiles, activeProfiles("id") === friends("profile"))
      .groupBy("profile")
      .count()
      .show(showLimit)
  }

  def viewAggregateCommentsPerPost(userId: Int) {
    println("viewAggregatePerPost")
    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")

    val commentsFromUsers = comments
      .filter(col("from_id") > 0)
      .groupBy(col("id"),
        col("post_owner_id"),
        col("from_id"))
      .count()

    val friends = spark.read.parquet(s"$dir\\friends.parquet")
      .filter(col("profile") === userId)

    val postsCommentStat = friends
      .join(commentsFromUsers,
        commentsFromUsers("from_id") === friends("follower").cast("int") &&
          commentsFromUsers("post_owner_id") === friends("profile").cast("int"))
      .groupBy("profile")
      .agg(sum("count").alias("friends_comments"),
        max("count").alias("friends_comments_max"),
        mean("count").alias("friends_comments_mean"))
      .withColumnRenamed("profile", "from_id")
      .show(showLimit)
  }

  def viewAggregateLikesPerPost(userId: Int) {
    println("viewAggregateLikesPerPost")
    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val likesFromUsers = likes
      .filter(col("likerId") > 0)
      .groupBy(col("itemId"), col("ownerId"), col("likerId"))
      .count()

    val friends = spark.read.parquet(s"$dir\\friends.parquet")
      .filter(col("profile") === userId)

    val postsLikesStat = friends
      .join(likesFromUsers,
        likesFromUsers("likerId") === friends("follower").cast("int") &&
          likesFromUsers("ownerId") === friends("profile").cast("int"))
      .groupBy("profile")
      .agg(sum("count").alias("friends_likes"),
        max("count").alias("friends_likes_max"),
        mean("count").alias("friends_likes_mean"))
      .withColumnRenamed("profile", "from_id")
      .show(showLimit)
  }

  def viewAggregateCommentsPerUser(userId: Int) {
    println("viewAggregateCommentsPerUser")

    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")

    val commentsFromUsers = comments
      .filter(col("from_id") > 0)
      .groupBy(col("id"), col("post_owner_id"), col("from_id"))
      .count()

    val followers = spark.read.parquet(s"$dir\\followers.parquet")
      .filter(col("profile") === userId)

    val aggregateCommentsPerUser = followers
      .join(commentsFromUsers,
        commentsFromUsers("from_id") === followers("follower").cast("int") &&
          commentsFromUsers("post_owner_id") === followers("profile").cast("int"))
      .groupBy("profile")
      .agg(sum("count").alias("followers_comments"),
        max("count").alias("followers_comments_max"),
        mean("count").alias("followers_comments_mean"))
      .withColumnRenamed("profile", "from_id")
      .show(showLimit)
  }

  def viewAggregateLikesPerUser(userId: Int) {
    println("viewAggregateLikesPerUser")

    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val likesFromUsers = likes
      .filter(col("likerId") > 0)
      .groupBy(col("itemId"), col("ownerId"), col("likerId"))
      .count()

    val followers = spark.read.parquet(s"$dir\\followers.parquet")
      .filter(col("profile") === userId)

    val aggregateLikesPerUser = followers
      .join(likesFromUsers,
        likesFromUsers("likerId") === followers("follower").cast("int") &&
          likesFromUsers("ownerId") === followers("profile").cast("int"))
      .groupBy("profile")
      .agg(sum("count").alias("followers_likes"),
        max("count").alias("followers_likes_max"),
        mean("count").alias("followers_likes_mean"))
      .withColumnRenamed("profile", "from_id")
      .show(showLimit)
  }

  def viewEmoji(userId: Int) {
    /* println("viewEmoji")

     val neg = spark.read.csv("neg.csv")
     val lst_neg = neg.rdd.map(r => r.getString(0)).collect.toList
     val neg_emo: (String) => (Int) = _text => {
       val inside = EmojiParser.extractEmojis(_text).asScala
       var nneg = 0
       for (_cur <- inside) {
         if (lst_neg.contains(_cur)) {
           nneg += 1
         }
       }
       nneg
     }
     val neg_udf = udf(neg_emo)

     val pos = spark.read.csv("pos.csv")
     val lst_pos = pos.rdd.map(r => r.getString(0)).collect.toList
     val pos_emo: (String) => (Int) = _text => {
       val inside = EmojiParser.extractEmojis(_text).asScala
       var npos = 0
       for (_cur <- inside) {
         if (lst_pos.contains(_cur)) {
           npos += 1
         }
       }
       npos
     }
     val pos_udf = udf(pos_emo)

     val neu = spark.read.csv("neu.csv")
     val lst_neu = neu.rdd.map(r => r.getString(0)).collect.toList
     val neu_emo: (String) => (Int) = _text => {
       val inside = EmojiParser.extractEmojis(_text).asScala
       var nneu = 0
       for (_cur <- inside) {
         if (lst_neu.contains(_cur)) {
           nneu += 1
         }
       }
       nneu
     }
     val neu_udf = udf(neu_emo)

     val posts = spark.read.parquet(s"$dir\\userWallPosts.parquet")
     val postsNeg = posts.withColumn("Emoji_neg", neg_udf(col("text")))
     val postsPos = postsNeg.withColumn("Emoji_pos", pos_udf(col("text")))
     val postsNeu = postsPos.withColumn("Emoji_neu", neu_udf(col("text")))
       .select("from_id", "Emoji_pos", "Emoji_neu", "Emoji_neg")


     val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")
     val commentsNeg = comments.withColumn("Emoji_neg", neg_udf(col("text")))
     val commentsPos = commentsNeg.withColumn("Emoji_pos", pos_udf(col("text")))
     val commentsNeu = commentsPos.withColumn("Emoji_neu", neu_udf(col("text")))
       .select("from_id", "Emoji_pos", "Emoji_neu", "Emoji_neg")

     val all = postsNeu.union(commentsNeu)
     all
       .groupBy("from_id")
       .sum()
       .select("from_id", "sum(Emoji_pos)", "sum(Emoji_neu)", "sum(Emoji_neg)")
       .show(showLimit)*/
  }

  def viewCountComments(userId: Int) {
    println("countComments")
    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")
    comments
      .groupBy("post_owner_id")
      .count()
      .filter(col("post_owner_id") === userId)
      .show(showLimit)
  }


  def viewCountAllPosts(userId: Int) {
    println("CountAllPosts")
    val posts = spark.read.parquet(s"$dir\\userWallComments.parquet")
    posts
      .groupBy("from_id")
      .count()
      .show(showLimit)
  }

  def viewCountOriginalPosts(userId: Int) {
    println("CountOriginalPosts")
    val allPosts = spark.read.parquet(s"$dir\\userWallPosts.parquet")
    allPosts
      .groupBy("from_id")
      .count()
      .filter(col("from_id") === userId)
      .show(showLimit)
  }

  def viewCountReposts(userId: Int) {
    println("CountReposts")
    val allPosts = spark.read.parquet(s"$dir\\userWallPosts.parquet")
    allPosts
      .filter(col("is_reposted") === true)
      .groupBy("from_id")
      .count()
      .filter(col("from_id") === userId)
      .show(showLimit)
  }

  def viewCountLikes(userId: Int) {
    println("CountLikes")
    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    likes
      .groupBy("likerId")
      .count()
      .filter(col("likerId") === userId)
      .show(showLimit)
  }


  def viewCountFriends(userId: Int) {
    println("CountFriends")
    val likes = spark.read.parquet(s"$dir\\friends.parquet")
    likes
      .groupBy("profile")
      .count()
      .filter(col("profile") === userId)
      .show(showLimit)
  }

  def viewCountGroups(userId: Int) {
    println("CountFriends")
    val likes = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")
    likes
      .groupBy("user")
      .count()
      .filter(col("user") === userId)
      .show(showLimit)
  }

  def viewCountFollowers(userId: Int) {
    println("CountFollowers")
    val followers = spark.read.parquet(s"$dir\\followers.parquet")
    followers.sort(desc("profile")).show(showLimit)
    followers
      .filter(col("profile") !== 0)
      .filter(col("profile") === userId)
      .groupBy("profile")
      .count() //java.io.IOException: could not read page Page [bytes.size=1048592, valueCount=131073, uncompressedSize=1048592] in col [profile] INT64
      .show(showLimit)
  }


  def viewCountVideos(userId: Int) {
    println("CountVideos")
    val followerProfiles = spark.read.parquet(s"$dir\\followerProfiles.parquet")
    followerProfiles
      .filter(col("id") === userId)
      .select(col("counters.videos"))
      .show(showLimit)
  }

  def viewCountAudios(userId: Int) {
    println("CountAudios")
    val followerProfiles = spark.read.parquet(s"$dir\\followerProfiles.parquet")
    followerProfiles
      .filter(col("id") === userId)
      .select(col("counters.audios"))
      .show(showLimit)
  }

  def viewCountPhotos(userId: Int) {
    println("viewCountPhotos")
    val followerProfiles = spark.read.parquet(s"$dir\\followerProfiles.parquet")
    followerProfiles
      .filter(col("id") === userId)
      .select(col("counters.photos"))
      .show(showLimit)
  }

  def viewCountGifts(userId: Int) {
    println("viewCountGifts")
    val followerProfiles = spark.read.parquet(s"$dir\\followerProfiles.parquet")
    followerProfiles
      .filter(col("id") === userId)
      .select(col("counters.gifts"))
      .show(showLimit)
  }


  def viewCountIncomingCommentsOtherUsers(userId: Int) {
    println("CountIncomingCommentsOtherUsers")
    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")

    val incoming = comments
      .filter(col("from_id") === userId)
      .filter(col("from_id") =!= col("post_owner_id"))
      .groupBy("post_owner_id", "post_id")
      .count()
      .show(showLimit)
  }

  def viewMaxIncomingCommentsPerPost(userId: Int) {
    println("MaxIncomingCommentsPerPost")
    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")

    val incoming = comments
      .filter(col("from_id") === userId)
      .filter(col("from_id") =!= col("post_owner_id"))
      .groupBy("post_owner_id", "post_id")
      .count()
      .agg(max("count"))
      .show(showLimit)
  }

  def viewMeanIncomingCommentsPerPost(userId: Int) {
    println("MeanIncomingCommentsPerPost")
    val comments = spark.read.parquet(s"$dir\\userWallComments.parquet")

    val incoming = comments
      .filter(col("from_id") === userId)
      .filter(col("from_id") =!= col("post_owner_id"))
      .groupBy("post_owner_id", "post_id")
      .count()
      .agg(mean("count"))
      .show(showLimit)
  }


  def viewCountIncomingLikesPerPost(userId: Int) {
    println("CountIncomingLikesPerPost")
    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val incoming = likes
      .filter(col("likerId") === userId)
      .filter(col("likerId") =!= col("ownerId"))
      .groupBy("likerId", "itemId")
      .count()
      .show(showLimit)
  }

  def viewMaxIncomingLikesPerPost(userId: Int) {
    println("MaxIncomingLikesPerPost")
    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val incoming = likes
      .filter(col("likerId") === userId)
      .filter(col("likerId") =!= col("ownerId"))
      .groupBy("likerId", "itemId")
      .count()
      .agg(max("count"))
      .show(showLimit)
  }

  def viewMeanIncomingLikesPerPost(userId: Int) {
    println("MeanIncomingLikesPerPost")
    val likes = spark.read.parquet(s"$dir\\userWallLikes.parquet")
    val incoming = likes
      .filter(col("likerId") === userId)
      .filter(col("likerId") =!= col("ownerId"))
      .groupBy("likerId", "itemId")
      .count()
      .agg(mean("count"))
      .show(showLimit)
  }

  def viewCountGeoTaggedPosts(userId: Int) {
    println("CountGeoTaggedPosts")
    val posts = spark.read.parquet(s"$dir\\userWallPosts.parquet")
    val geo = posts
      .filter(col("geo").isNotNull)
      .groupBy("from_id")
      .count()
      .filter(col("from_id") === userId)
      .show(showLimit)
  }


  def viewCountPublicGroupsForUser(userId: Int) {
    println("CountPublicGroupsForUser")
    val groupsProfiles = spark.read.parquet(s"$dir\\groupsProfiles.parquet")
    val allUserGroups = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")
    val userGroups = allUserGroups
      .filter(col("user") === userId)
    val userGroupsWithProfiles1 = userGroups
      .join(groupsProfiles, col("id") === abs(col("group")), "inner")
      .filter(col("is_closed") === 0)
      .select(userGroups("key"))
      .distinct()
      .agg(count("key"))
      .show(showLimit)
  }

  def viewCountPrivateGroupsForUser(userId: Int) {
    println("CountPrivateGroupsForUser")
    val groupsProfiles = spark.read.parquet(s"$dir\\groupsProfiles.parquet")
    val allUserGroups = spark.read.parquet(s"$dir\\userGroupsSubs.parquet")
    val userGroups = allUserGroups
      .filter(col("user") === userId)
    val userGroupsWithProfiles = userGroups
      .join(groupsProfiles, col("id") === abs(col("group")), "inner")
      .filter(col("is_closed") === 1)
      .select(userGroups("key"))
      .distinct()
      .agg(count("key"))
      .show(showLimit)
  }
}